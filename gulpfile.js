var gulp = require('gulp');
var less = require('gulp-less');

gulp.task('styles', function(){
   gulp.src('global.less')
       .pipe(less())
       .pipe(gulp.dest('css'));
});

gulp.task('watch', function(){
    gulp.watch('global.less',['styles']);
});